# Créer simplement des pages web avec le modèle Pandoc

## Que fait ce modèle ?

Ce modèle propose un outil simple pour transformer des fichiers textes écrits avec la syntaxe Markdown en pages HTML, grâce à [Pandoc](https://pandoc.org/).

Vous avez simplement à comprendre comment fonctionne la syntaxe Markdown, l'outil s'occupe automatiquement de la conversion de votre fichier en une page web.

Un exemple : le fichier [index.md](https://forge.aeif.fr/modeles-projets/modele-pandoc/-/blob/main/index.md?plain=1) de ce répertoire crée la page [index.html](https://modeles-projets.forge.aeif.fr/modele-pandoc/index.html).

## Comment utiliser ce modèle ?

Il suffit de réutiliser ce modèle en cliquant sur “Fork” ou “Créer une divergence” en français.

Chaque fichier markdown (avec une extension `.md`) que l'on créera dans son répertoire sera alors automatiquement converti en une page HTML, dans le même dossier ou sous-dossier, et avec le même nom (en supprimant l'extension `.md`).

Plus précisément, supposons que votre nom d'utilisateur est `mon_nom` et que vous avez récupéré ce modèle en nommant votre répertoire `mon_répertoire`.

Vous avez ensuite créé un fichier `mon_fichier.md` à la racine de ce répertoire.

Dans ce cas, votre page web se trouvera à l'adresse suivante : `https://mon_nom.forge.aeif.fr/mon_répertoire/mon_fichier`.

Si `mon_fichier.md` est dans le dossier `mon_dossier/`, votre page web se trouvera à l'adresse : `https://mon_nom.forge.aeif.fr/mon_répertoire/mon_dossier/mon_fichier`

