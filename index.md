# Les cours donnés au lycée Charles de Foucauld, Paris 75018

## [Informatique](informatique/index.html)

  - [Première](informatique/premiere-nsi/)
  - [Terminale](informatique/terminale-nsi/)

## [Mathématiques](mathematiques/index.html)

  - [Seconde](mathematiques/seconde/)
  - [Première STMG](mathematiques/premiere-stmg/)
  - [Mathématiques expertes](mathematiques/terminale-s/)
  - [BTS](mathematiques/bts/)
